<html>
<head>
	
	<title><?php wp_title(''); ?></title>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>	
	<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.min.js"></script>	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>
	
</head>
<body>
	<div class="row">
		<div class="large-12 columns">
			<?php if ( get_header_image() ) : ?>
				<div id="site-header">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
					</a>
				</div>
			<?php endif; ?>
			<h1><?php bloginfo('name'); ?></h1>
			<p><?php bloginfo('description'); ?><p>
			
		</div>
		<hr></hr>
		<div class="large-12 columns">
			<dl class="sub-nav large-5 columns"> 
				<dd><a href="<?php echo home_url(); ?>">Home</a></dd> 
				<dd><a href="#">Sobre</a></dd> 
				<dd><a href="#">Contato</a></dd> 
			</dl>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('search')) :?><?php endif; ?>
		</div>	
		<hr></hr>
	</div>
	