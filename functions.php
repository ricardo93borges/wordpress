<?php
/* WIDGETS */
 
if (function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => 'sidebar',
		'id'   => 'sidebar',
		'description'   => 'isso � uma sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div><hr></hr>',
		'before_title'  => '<p class="text-center" style="margin:2px 0">',
		'after_title'   => '</p>'
	));
	
	register_sidebar(array(
		'name' => 'search',
		'id'   => 'search',
		'description'   => 'isso � um formulario de pesquisa',
		'before_widget' => '<div id="%1$s" class="large-5 columns right">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="text-center" style="margin:2px 0">',
		'after_title'   => '</p>'	
	));

}
 

function get_first_post_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = get_template_directory_uri()."/images/default.jpg";
  }
  return $first_img;
}

function get_only_post_text(){
	$content = get_the_content();
	$content = preg_replace('/(<)([img])(\w+)([^>]*>)/', �, $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	$content = strip_tags($content);
	$content = substr($content, 1, 100)."[...]";
	return $content;
}
?>