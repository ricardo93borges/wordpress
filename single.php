
<?php get_header(); ?>

<div class="row">
	<div class="large-10 columns">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<h1><?php the_title(); ?></h1>
			<p>
				Postado por <?php the_author() ?> em <?php the_time('d/M/Y') ?> - 
				<?php comments_popup_link('Sem Comentários', '1 Comentário', '% Comentários', 'comments-link', ''); ?> 
				<?php edit_post_link('(Editar)'); ?>
			</p>
			<p><?php the_content(); ?></p>
			<hr></hr>
	</div>
	<?php get_sidebar(); ?>
	<div class="large-10 columns left">
		<?php comments_template(); ?>
	</div>
	
		<?php endwhile?>
		<?php else: ?>
			<div class="large-8 columns">
				<div class="artigo">
					<h2>Nada Encontrado</h2>
					<p>Erro 404</p>
				</div>            
			</div>
		<?php endif; ?>

</div>
<?php get_footer(); ?>
