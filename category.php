
<?php get_header(); ?>

<div class="row">
	<div class="large-10 columns">
		<ul class="inline-list">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<li class="large-3 columns" style="margin:0;">
				<strong class="text-center"><?php the_title(); ?></strong>
				<a href="<?php the_permalink() ?>">
					<img class="th radius" src="<?php echo get_first_post_image(); ?>"/>
				</a>
				<p class="text-center">
					<?php echo get_only_post_text() ?>
					<a href="<?php the_permalink() ?>">Continue lendo</a>
				</p>
			</li>
			<?php endwhile?>
			<?php else: ?>
                <div class="artigo">
                    <h2>Nada Encontrado</h2>
                    <p>Erro 404</p>
                    <p>Sem posts no momento.</p>
                </div>            
            <?php endif; ?>
			
		</ul>
	</div>
<?php get_sidebar(); ?>

</div>
<?php get_footer(); ?>
